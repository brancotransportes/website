#!/bin/bash

DATE="[$(date +"%Y-%m-%d_%H:%M:%S")]"
BACKUPFOLDER="/media/Storage/Temporary"



echo "BCL Website: >> Creating a new snapshot! <<"


touch "${DATE}.snapshot-date"
7z a "$BACKUPFOLDER/BCL_Website_$DATE.7z" . -mx9 -mmt=on -mhe
rm *.snapshot-date


SUM=$(md5sum "$BACKUPFOLDER/BCL_Website_$DATE.7z" | awk '{ print $1 }') ; echo $SUM $DATE
mv "$BACKUPFOLDER/BCL_Website_$DATE.7z" "$BACKUPFOLDER/BCL_Website_${DATE}__$SUM.7z"

echo "BCL Website: >> SNAPSHOT CREATED! <<"
sleep 30s
